from ast import operator
from ..arithmetic_operator import ArithmeticOperation

operator = ArithmeticOperation(1,2)
assert operator.add() == 3
