Command to execute a module file

python3 -m ourprog.test.sample_test #(This will work)

cd ourprog/test

python3 sample_test.py #(This will not work)

This would result in the below error

# File "/Users/rajeshvaran/learning/data_testing_spikes/package_spike/ourprog/ourprog/test/sample_test.py", line 2, in <module>
#    from ..arithmetic_operator import ArithmeticOperation
#ImportError: attempted relative import with no known parent package