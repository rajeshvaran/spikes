import os
import pathlib
import pandas as pd
import pytest
from pandas.testing import assert_frame_equal
from dotenv import load_dotenv
from snowflake_connect import SnowflakeDataConnection


@pytest.fixture
def setup():
    test_data_files = 'file://' + str(pathlib.Path(__file__).parent.resolve()) + '/sample_csv_files/*.csv'
    database_name = "sf_tuts"
    dataware_house = "sf_tuts_wh"
    table_name = 'emp_basic'
    table_definition = "CREATE OR REPLACE TABLE "\
                        "emp_basic(first_name string, last_name string, email string, "\
                        "streetaddress string, city string, start_date date);"

    load_dotenv()
    snowflake_connector = SnowflakeDataConnection(os.environ['USER_NAME'], os.environ['PASSWORD'], os.environ['ACCOUNT_NAME'])
    snowflake_connector.create_database(database_name=database_name)
    snowflake_connector.create_table(database_name, table_definition)
    snowflake_connector.create_warehouse(dataware_house)        
    snowflake_connector.stage_and_copy_into_table(file_path=test_data_files, table_name=table_name)
    yield snowflake_connector
    snowflake_connector.close_all()
    
    
def test_actual_employee_list_is_as_expected(setup : SnowflakeDataConnection):
    select_employee_query =  "select *from emp_basic order by first_name,last_name"                       
    expected_csv = str(pathlib.Path(__file__).parent.resolve()) + '/test_data/expected.csv'                
    actual =setup.get_result_for(select_employee_query)
    expected = pd.read_csv(expected_csv)
    # Converstion to string else comparison fails for date alone. 
    actual["START_DATE"] = actual["START_DATE"].astype("str")
    assert_frame_equal(expected,actual)
