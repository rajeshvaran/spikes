import snowflake.connector


class SnowflakeDataConnection:
  def __init__(self,user_name, password, account_name):
    self.snowflake_connection = snowflake.connector.connect(
    user=user_name,
    password=password,
    account=account_name

    ) 
    self.snowflake_cursor = self.snowflake_connection.cursor()

  def current_version(self):
    self.snowflake_cursor.execute("SELECT current_version()")
    result = self.snowflake_cursor.fetchone()
    return result[0]

  def create_database(self, database_name):
    self.snowflake_cursor.execute("CREATE DATABASE IF NOT EXISTS " + database_name)
    result=self.snowflake_cursor.fetchone()
    return result

  
  def create_table(self, database_name, table_definition):
    self.snowflake_cursor.execute("USE DATABASE " + database_name)
    self.snowflake_cursor.execute(table_definition)
    result=self.snowflake_cursor.fetchone()
    return result

  def create_warehouse(self,ware_house_name):
    ware_house_creation_string = "CREATE WAREHOUSE IF NOT EXISTS  {ware_house_name} with "\
    "warehouse_size='X-SMALL' "\
    "auto_suspend = 180 "\
    "auto_resume = true "\
    "initially_suspended=true;".format(ware_house_name = ware_house_name)
    self.snowflake_cursor.execute(ware_house_creation_string)
    result=self.snowflake_cursor.fetchone()
    print(result[0])
    self.snowflake_cursor.execute("select current_warehouse();")
    result=self.snowflake_cursor.fetchone()
    return result
  def stage_and_copy_into_table(self, file_path, table_name):
    self.snowflake_cursor.execute("PUT " + file_path + " @%" + table_name) 
    self.snowflake_cursor.execute("COPY INTO " + table_name + " from @%" + table_name + " file_format = (type = csv field_optionally_enclosed_by='\"')")
  
  def get_result_for(self, query):
    self.snowflake_cursor.execute(query)
    return self.snowflake_cursor.fetch_pandas_all()

  def close_all(self):
    self.snowflake_cursor.close()
    self.snowflake_connection.close()

if __name__ == "__main__":
  import os
  from dotenv import load_dotenv
  import pathlib
  load_dotenv()
  sc = SnowflakeConnection(os.environ['USER_NAME'], os.environ['PASSWORD'], os.environ['ACCOUNT_NAME'])
  try:    
    print(sc.current_version())
    database_name = "sf_tuts"
    print(sc.create_database(database_name=database_name))
    print(sc.create_table(database_name, "CREATE OR REPLACE TABLE "
                    "emp_basic(first_name string, last_name string, email string, "
                    "streetaddress string, city string, start_date date);"))
              
    print(sc.create_warehouse("sf_tuts_wh"))
    file_path='file://' + str(pathlib.Path(__file__).parent.resolve()) + '/sample_csv_files/*.csv'
    table_name = 'emp_basic'
    sc.stage_and_copy_into_table(file_path=file_path, table_name=table_name)
    print(sc.get_result_for("select *from emp_basic order by first_name,last_name"))
  finally:
    sc.close_all()


# con.cursor().execute("PUT file:///tmp/data/file* @%testtable")

# put file:///tmp/employees0*.csv @sf_tuts.public.%emp_basic;
# put file://c:\temp\employees0*.csv @sf_tuts.public.%emp_basic;