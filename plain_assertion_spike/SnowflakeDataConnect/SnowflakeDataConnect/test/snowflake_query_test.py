import os
import pathlib
import pandas as pd
from pandas.testing import assert_frame_equal
from dotenv import load_dotenv
from SnowflakeDataConnect.snowflake_connect import SnowflakeDataConnection

def test_actual_employee_list_is_as_expected():
    load_dotenv()
    sc = SnowflakeDataConnection(os.environ['USER_NAME'], os.environ['PASSWORD'], os.environ['ACCOUNT_NAME'])
    test_data_files = 'file://' + str(pathlib.Path(__file__).parent.resolve()) + '/sample_csv_files/*.csv'
    database_name = "sf_tuts"
    dataware_house = "sf_tuts_wh"
    table_name = 'emp_basic'
    table_definition = "CREATE OR REPLACE TABLE "\
                        "emp_basic(first_name string, last_name string, email string, "\
                        "streetaddress string, city string, start_date date);"
    select_employee_query =  "select *from emp_basic order by first_name,last_name"                       
    expected_csv = str(pathlib.Path(__file__).parent.resolve()) + '/test_data/expected.csv'

    try:
        print(sc.current_version())
        sc.create_database(database_name=database_name)
        sc.create_table(database_name, table_definition)
        sc.create_warehouse(dataware_house)        
        sc.stage_and_copy_into_table(file_path=test_data_files, table_name=table_name)
        actual =sc.get_result_for(select_employee_query)
        expected = pd.read_csv(expected_csv, parse_dates=['START_DATE'])

        # Converstion to datetime else comparison fails for date alone attributed date interpreted as object 
        actual["START_DATE"] = actual["START_DATE"].astype("datetime64")
        assert_frame_equal(expected,actual)        
    finally:
        sc.close_all()

# python -m SnowflakeDataConnect.test.snowflake_query_test
if __name__ == "__main__":
    test_actual_employee_list_is_as_expected()
